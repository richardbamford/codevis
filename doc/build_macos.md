## Mac build

First, setup Craft following the information provided here:
`https://community.kde.org/Get_Involved/development/Mac`

Then build Codevis:

```
source ~/CraftRoot/craft/craftenv.sh
craft codevis
```
