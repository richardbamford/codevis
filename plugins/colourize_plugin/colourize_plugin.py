def recoulourize(actionHandler):
    pigmentRed = Color(237, 27, 36, 255)
    viewedEntities = actionHandler.getAllEntitiesInCurrentView()
    for viewedEntity in viewedEntities:
        viewedEntity.setColor(pigmentRed)

def hookGraphicsViewContextMenu(menuHandler):
    menuHandler.registerContextMenu("Recolourize", recoulourize)
